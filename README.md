# Paper Trail
Paper trail is a 2D pixel art platformer. I started this project with the intention of creating a full game but time-constraints and losing interest is going to be the end of it. So I'm open-sourcing it. The project is still in proto-typing stage so code isn't neatly organized or anything.

The purpose of the game was to explore a factory as a little paper that has come to life and then avoid hazards that could kill you (fire, water etc.) while being able to be blown around by wind currents or thrown around while curled up into a paper ball.

# Get it running
Getting it running is simple

1. Clone the repo
2. Download the Godot 3 engine from https://godotengine.org/download/
3. Import the project folder in Godot
4. Open the project in Godot
5. Press the play button

There are a few screens to move between, but I never got to designing proper platforming sections.

# Controls
Uses arrow keys for moving around, spacebar to jump and shift to curl up into a ball. On an XBox gamepad, A is jump and B is curl into ball.

You can look at a little gameplay snippet over here: https://gfycat.com/WaryAncientBoaconstrictor

# Notes on the code
Just feel its worth mentioning that I was trying some memory management stuff (Not really a problem with the size of the assets I'm working with, but was just a bit of an experiment), so the scene transitioning things might seem a little contrived.